<table align="center">
<tr><td align="center" width="10000">


# <strong> FDMNES X-ray Spectroscopy Examples </strong>

<p>
    <a href="https://fdmnes.neel.cnrs.fr">FDMNES</a>
</p>

</td></tr></table>

#

This repository provides example calculations for X-ray spectroscopy within FDMNES. 

Further details about FDMNES can be found:

*O. Bunau and Y. Joly “Self-consistent aspects of x-ray absorption calculations“ J. Phys.: Condens. Matter 21, 345501 (2009).

*S. A. Guda, A. A. Guda, M. A. Soldatov, K. A. Lomachenko , A. L. Bugaev, C. Lamberti, W. Gawelda, C. Bressler, G. Smolentsev, A. V. Soldatov, Y. Joly “Optimized Finite Difference Method for the Full-Potential XANES Simulations: Application to Molecular Adsorption Geometries in MOFs and Metal-Ligand Intersystem Crossing Transients“ J. Chem. Theory Comput. 11, 4512-4521 (2015).



## Getting Started

The quickest way to get started this is to clone this repository:


<!---
```
git clone https://gitlab.com/penfoldtj/FDMNES-XAS.git
```
--->

```
git clone https://gitlab.com/penfoldtj/FDMNES-XAS.git
```

This contains all the source files as well as example input files.


## Questions

Feel free to get in touch if you have any questions at tom.penfold@newcastle.ac.uk

